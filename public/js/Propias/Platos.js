let platos = {
    popup_vista_platos: function (id_plato, id_mesa) {
        $.ajax({
            url: root + 'plato/' + id_plato + '/ver-descripcion/' + id_mesa,
            type: "GET",
        }).done(function (r) {
            let contenedor = $("#platos_modal");
            contenedor.html(r);
            contenedor.modal({backdrop: 'static'});
        }).fail(function (r) {
            console.log(r);
        })
    },
    vista_platos: function (id_plato, id_mesa, es_menu) {
        $.ajax({
            url: root + 'plato/' + id_plato + '/ver-descripcion/' + id_mesa,
            type: "GET",
            data: {
                esMenu: es_menu
            }
        }).done(function (r) {
            let contenedor = $("#plato_detalle");
            contenedor.html(r);
        }).fail(function (r) {
            console.log(r);
        })
    },
    vista_platos_busqueda: function (es_menu, id_menu) {
        console.log(es_menu);
        let id_categoria = $('#buscador_categorias').val();
        let esconder_menu = id_categoria == -2;
        let url = esconder_menu ? root + '/platos/vista-promocion/' + es_menu + '/' + id_menu : root + '/platos/vista-platos/' + id_categoria + '/' + es_menu + '/' + id_menu;
        let contenedor_visible = esconder_menu ? $('#vista-categorias-promociones') : $('#vista-categorias-platos');
        let contenedor_invisible = !esconder_menu ? $('#vista-categorias-promociones') : $('#vista-categorias-platos');
        if (id_categoria == 0) {
            $.ajax({
                url: root + '/platos/vista-promocion/' + es_menu + '/' + id_menu,
                type: "GET",
            }).done(function (r) {
                $('#vista-categorias-promociones').html(r);
                $('#vista-categorias-promociones').show();
            }).fail(function (r) {
                console.log(r);
            });

            $.ajax({
                url: root + '/platos/vista-platos/' + 0 + '/' + es_menu + '/' + id_menu,
                type: "GET",
            }).done(function (r) {
                $('#vista-categorias-platos').html(r);
                $('#vista-categorias-platos').show();
            }).fail(function (r) {
                console.log(r);
            });
        } else {
            $.ajax({
                url: url,
                type: "GET",
            }).done(function (r) {
                contenedor_visible.html(r);
                contenedor_visible.show();
                contenedor_invisible.hide();
            }).fail(function (r) {
                console.log(r);
            })
        }
    },
    vista_agregar_plato: function (idPlatoPromo, tipo) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        });
        $.ajax({
            url: root + 'plato/agregar',
            type: "POST",
            data: {
                token: csrfToken,
                idPlatoPromo: idPlatoPromo,
                tipo: tipo
            }
        }).done(function (r) {
            let contenedor = $("#plato_detalle");
            contenedor.html(r);
        }).fail(function (r) {
            console.log(r);
        })
    },
    popup_vista_agregar_plato: function (idPlatoPromo, tipo) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        });
        $.ajax({
            url: root + 'plato/agregar',
            type: "POST",
            data: {
                token: csrfToken,
                idPlatoPromo: idPlatoPromo,
                tipo: tipo
            }
        }).done(function (r) {
            let contenedor = $("#platos_modal");
            contenedor.html(r);
            contenedor.modal();
        }).fail(function (r) {
            console.log(r);
        })
    },
    vista_resgistro_platos: function (tipo) {
        let plato = $('#formulario_agregar_plato');
        let promo = $('#formulario_agregar_promo');
        console.log(plato);
        console.log(promo);
        if (tipo == 'promo') {
            plato.hide();
            plato.find(':input').prop('disabled', true);
            promo.find(':input').prop('disabled', false);
            promo.show();
        } else if (tipo == 'plato') {
            promo.hide();
            promo.find(':input').prop('disabled', true);
            plato.find(':input').prop('disabled', false);
            plato.show();
        }
    },
    cambiar_cantidad_guarnicion: function (plato) {
        let cantidad = $('#' + plato.id).val();
        let contenedor = $('#guarniciones_plato').children('input');
        let contador = 0;
        let restar = true;
        Array.prototype.forEach.call(contenedor, function (e) {
            contador += parseInt(e.value);
        });
        if (contador > cantidad) {
            Array.prototype.forEach.call(contenedor, function (e) {
                if (restar) {
                    if (e.value > 0) {
                        restar = false;
                        e.max = parseInt(e.value) - 1;
                        e.value = parseInt(e.value) - 1;
                    }
                }
            });
        } else {
            contenedor[0].max = parseInt(contenedor[0].value) + 1;
            if (!parseInt(contenedor[0].value) == 0) {
                contenedor[0].value = parseInt(contenedor[0].value) + 1;
            }
        }
    },
    validar_guardicion: function () {
        let cantidad_plato = parseInt($('#pedido_mesa').val());
        let contenedor = $('#guarniciones_plato').children('input');
        let contador = 0;
        Array.prototype.forEach.call(contenedor, function (e) {
            contador += parseInt(e.value);
        });
        if (cantidad_plato == contador) {
            Array.prototype.forEach.call(contenedor, function (e) {
                e.max = e.value;
            });
        } else if (cantidad_plato > contador) {
            Array.prototype.forEach.call(contenedor, function (e) {
                e.max = parseInt(e.value) + 1;
            });
        }
    },
    recargar_menu: function (id_mesa, es_menu) {
        $.ajax({
            url: root + 'platos/' + es_menu + '/ver-todos/' + id_mesa,
            type: "GET",
        }).done(function (r) {
            let contenedor = $("#plato_detalle");
            contenedor.html(r);
        }).fail(function (r) {
            console.log(r);
        });
    }
};