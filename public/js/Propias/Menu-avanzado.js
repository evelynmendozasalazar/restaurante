let menuAvanzado = {
    popup_vista_avanzada: function (tipo, id=0) {
        let tipo_editado=tipo.replace(new RegExp('_', 'g'),'-');
        $.ajax({
            url: root+'menu-avanzado/'+tipo_editado+'/'+id,
            type: "GET",
        }).done(function (r){
            if(r==100){
                alert("No existe Tasa de medicion");
            } else {
                let contenedor = $("#menu_avanzado_modal");
                contenedor.html(r);
                contenedor.modal();
            }
        }).fail(function (r) {
            console.log(r);
        })
    },

    agregarMateriaPrima: function () {
        let contenedorPadre=$("#fieldset_materia_prima");
        let agregados = $('.js-example-basic-usage');
        let ids=[];
        agregados.each(function () {
           ids.push($( this ).val());
           $( this ).parent().addClass('disabled-select');
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        });
        $.ajax({
            url: root+'obtenerMateriasPrimasParaGuarnicion',
            type: "POST",
            data:{
                'ids': ids
            }
        }).done(function (r){
            contenedorPadre.append(r);
        }).fail(function (r) {
            console.log(r);
        });
    },

    borrarMateriPrima: function (id){
        let div_borrar=$(id);
        div_borrar.remove();
        let agregados = $('.js-example-basic-usage');
        agregados.last().parent().removeClass('disabled-select');
    },

    cambiarEstado: function (tipo, id, cambiarEstado) {
        let tipo_editado=tipo.replace(new RegExp('_', 'g'),'-');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        });
        $.ajax({
            url: root+'guardar/'+tipo_editado+'/'+id+'/'+cambiarEstado,
            type: "POST",
        }).done(function (r){
            menuAvanzado.refrescar(tipo, tipo+'_'+id);
        }).fail(function (r) {
            console.log(r);
        })
    },

    refrescar: function (tipo, id) {
        let url= root+'menu-avanzado';
        $(location).attr("href", url);
    }
};