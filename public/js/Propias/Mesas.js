let mesas = {
    popup_vista_mesas : function (id_mesa, nombre_mesa) {
        $.ajax({
            url: root+'vista-mesa/'+id_mesa+'/mesa/'+nombre_mesa,
            type: "GET",
        }).done(function (r){
            let contenedor = $("#mesas_modal");
            contenedor.html(r);
            contenedor.modal({backdrop: 'static'});
        }).fail(function (r) {
            console.log(r);
        })
    },
    PosicionVistaMesas : function () {
        let x=$('#tamanion').val();
        let y=$('#tamaniom').val();
        $.ajax({
            url: root+'mesas-posiciones',
            type: "GET",
            data:{
                columna: x,
                fila: y
            }
        }).done(function (r){
            let contenedor = $("#PosicionMesas");
            contenedor.html(r);
        }).fail(function (r) {
            console.log(r);
        })
    },
    habilitarDeshabilitarMesa : function (i, j) {
        let div=$('#MesaDiv_'+i+'_'+j);
        let input='#habilitadoDeshabilitado_'+i+'_'+j;
        let habilitado= $(input);
        if(habilitado.val() == 1){
            habilitado.val(0);
            div.fadeTo("slow", 0.5);
        }
        else{
            habilitado.val(1);
            div.fadeTo("slow", 1);
        }
    },
};