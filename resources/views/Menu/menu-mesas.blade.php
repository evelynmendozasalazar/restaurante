@include('Principales.header')
@section('content')
@stop
<div>
    <form method="POST" action="mesas-posiciones/guardar">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="exampleInputEmail1" style="float: left" class="col-4">Escoga Dimensiones Local</label>
            <div class="col-2" style="float: left">
                <input type="number" class="form-control" id="tamaniom"
                       aria-describedby="AyudaMesas" min="1" max="12" value="1">
                <small id="AyudaMesas" class="form-text text-muted">Cantidad de mesas en fila.</small>
            </div>
            <div class="col-2" style="float: left">
                <input type="number" class="form-control" id="tamanion"
                       aria-describedby="AyudaMesas2" min="1" max="12" value="1">
                <small id="AyudaMesas2" class="form-text text-muted">Cantidad de mesas en columna.</small>
            </div>
            <div class="col-4" style="float: left">
                <button type="button" onclick="mesas.PosicionVistaMesas()">Generar</button>
            </div>
        </div>
        <div style="clear: both" id="PosicionMesas">
            <?= $MesasInController->VerPosicionesDeMesas(true); ?>
        </div>
    </form>
</div>
@foreach($scriptsJs as $scriptJs )
    <?=  $scriptJs  ?>
@endforeach