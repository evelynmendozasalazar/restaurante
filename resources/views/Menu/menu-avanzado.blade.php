@include('Principales.header')
@section('content')
@stop
<ul class="nav nav-tabs">
    <li class="active nav-item"><a class="nav-link" data-toggle="tab" href="#categorias">Categorias</a></li>
    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#guarniciones">Guarniciones</a></li>
    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#materia_prima">Materia Prima</a></li>
    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tasa_medicion">Tasa de medicion</a></li>
    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#intereses">Intereses</a></li>
    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#trabajadores">Trabajadores</a></li>
</ul>
<div class="tab-content">
    <div id="categorias" class="tab-pane in active">
        <?= $Controller->getVistaTipoAgregar('Categorias'); ?>
    </div>
    <div id="guarniciones" class="tab-pane fade">
        <?= $Controller->getVistaTipoAgregar('Guarniciones'); ?>
    </div>
    <div id="materia_prima" class="tab-pane fade">
        <?= $Controller->getVistaTipoAgregar('MateriaPrima'); ?>
    </div>
    <div id="tasa_medicion" class="tab-pane fade">
        <?= $Controller->getVistaTipoAgregar('TasaDeMedicion'); ?>
    </div>
    <div id="intereses" class="tab-pane fade">
        <?= $Controller->getVistaTipoAgregar('Intereses'); ?>
    </div>
    <div id="trabajadores" class="tab-pane fade">
        <?= $Controller->getVistaTipoAgregar('Usuarios'); ?>
    </div>
</div>
<div class="modal fade" id="menu_avanzado_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>
<div class="modal fade" id="menu_avanzado_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>
@foreach($scriptsJs as $scriptJs )
    <?=  $scriptJs  ?>
@endforeach