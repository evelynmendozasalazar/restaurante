<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h3><label for="exampleInputEmail1" style="font-family:Trebuchet MS,Comic Sans MS,arial,Verdana,Sans-serif; color: blue; font-size: 25px;">GUARNICIONES</label></h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form method="POST" action="guardar/{{$tipo}}/{{$id}}" accept-charset="UTF-8" enctype="multipart/form-data">
            {{ csrf_field() }}
            <fieldset>
                <div class="col-12">
                    <div class="form-group">
                        <div class="col-12" style="text-align: center">
                            <label for="imagen">Imagen:</label>
                            <div style="text-align: center; padding: 3%">
                                <img id="previsualizar_imagen" style="width: 150px; height: 150px" alt="Sin imagen">
                            </div>
                            <a onclick="uploader.clickReal('imagen-input')"><i class="material-icons" style="cursor: pointer">add</i></a>
                            <input id="imagen-input" name="imagen-input" size="30" type="file" hidden/>
                            <!--small id="emailHelp" class="form-text text-muted">Nombre de la guarnicion.</small-->
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="col-12">
                    <div class="form-group">
                        <div class="col-12" style="float: left">
                            <label for="nombre" style="font-size: 18px; font-weight: bold">Nombre:</label>
                                <input type="text" maxlength="20" class="form-control" name="nombre" aria-describedby="emailHelp"
                                placeholder="Escriba el nombre">
                            <!--small id="emailHelp" class="form-text text-muted">Nombre de la guarnicion.</small-->
                        </div>
                    </div>
                </div>
            </fieldset>
            <br>
            <fieldset id="fieldset_materia_prima">
            </fieldset>
            <div style="text-align: right">
                @if($mostrar_agregar_materia_prima)
                    <a href="#" style="cursor: pointer" onclick="menuAvanzado.agregarMateriaPrima()">Agregar Materia Prima<i class="material-icons">add</i></a>
                @endif
            </div>
            <div class="col-12">
                <div class="align-content-center form-check" style="clear: both">
                    <input type="checkbox" class="form-check-input" name="activo" checked>
                    <label class="form-check-label" for="activo">Activo</label>
                </div>
                <p><fieldset style="text-align: center"><button type="submit" class="btn btn-default" style="background-color: #1883ba; color: #ffffff;">GUARDAR</button></fieldset></p>
            </div>
        </form>
    </div>
</div>

<script>
    $('#imagen-input').on('change',function (e) {
        uploader.previsualizarImagen(e);
    });
</script>