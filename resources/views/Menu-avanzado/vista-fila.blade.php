@if(!empty($tabla))
    @foreach($tabla as $row)
        <tr>
            <?php $cont=0; ?>
            @foreach($row as $key =>$item)
                @if($key!='hide')
                    @if($key=='activo')
                        <?php $cont++  ?>
                        @if($item)
                            <td><span class=" status text-success ">&bull;</span> Activo</td>
                        @else
                            <td><span class=" status text-danger ">&bull;</span> No Activo</td>
                        @endif
                    @elseif($key=="nombre")
                        <td><a onclick="menuAvanzado.popup_vista_avanzada('{{$ver_vista}}', {{$row['hide']['id']}});" style="cursor: pointer"><img src="/examples/images/avatar/1.jpg" class="avatar"> {{$item}}</a></td>
                    @else
                        <td>{{$item}}</td>
                    @endif
                @endif
            @endforeach
            @if($cont)
                @if($row['activo'])
                    <td><a style="cursor: pointer" onclick="menuAvanzado.cambiarEstado('{{$ver_vista}}',{{$row['hide']['id']}}, 0)" class="view" title="Desactivar{{$titulo}}" data-toggle="tooltip"><i class="material-icons">close</i></a></td>
                @else
                    <td><a style="cursor: pointer" onclick="menuAvanzado.cambiarEstado('{{$ver_vista}}',{{$row['hide']['id']}}, 1)" class="view" title="Activar{{$titulo}}" data-toggle="tooltip"><i class="material-icons">check</i></a></td>
                @endif
            @endif
        </tr>
    @endforeach
@endif