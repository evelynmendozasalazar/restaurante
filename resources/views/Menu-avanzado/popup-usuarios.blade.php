<div class="modal-dialog" role="document" style="max-width: 80%">
    <div class="modal-content">
        <div class="modal-header">
            <h3>Tasa de Medicion</h3>
        </div>
        <div>
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">medicion</label>
                    <input type="text" maxlength="20" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nombre entero Medicion">
                    <small id="emailHelp" class="form-text text-muted">Nombre de la medida.</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Abreviatura</label>
                    <input type="text" maxlength="5" class="form-control" id="exampleInputPassword1" placeholder="Abreviatura">
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" checked>
                    <label class="form-check-label" for="exampleCheck1">Activo</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>