<div style="float: left;" class="col-lg-4 col-md-12">
    <h1 style="text-align: center">orden mesa {{ $id }}</h1>
    <input type="hidden" name="mesa_id" value="{{ $id }}">
    <table class="table" style="width: 100%">
        <thead>
        <th style="width: 65%">Plato</th>
        <th style="width: 10%">cantidad</th>
        <th style="width: 12%">P/U</th>
        <th style="width: 13%">P/T</th>
        </thead>
        <tbody id="pedido_orden_mesa_"+{{$id}}>
        @foreach($orden as $ord)
            <tr>
                <td> {{ $ord['plato'] }}</td>
                <td> {{ $ord['cantidad'] }}</td>
                <td> {{ $ord['precio_unitario'] }}</td>
                <td>{{ $ord['precio_total'] }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div style="float: left">
</div>