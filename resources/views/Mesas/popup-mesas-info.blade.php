<div class="modal-dialog" role="document" style="max-width: 80%">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="header_modal">{{ $titulo }}</h4>
        </div>
        <div class="modal-body">
            <?= $MesasInController->vistaPedido($id_mesa)?>
            <div id="plato_detalle" class="col-lg-8 col-md-12" style="float: left; overflow-y: scroll; height: 300px">
                <?= $MenuPrincipalController->getPlatosActivos(false,$id_mesa); ?>
            </div>
            <div class="modal fade" id="platos_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>
        </div>
    </div>
</div>
