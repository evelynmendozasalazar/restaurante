<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrincipalController extends Controller
{
    public function index(){
        $this->initHelperCss();
        $this->passJsVariables();
        $scriptJs=$this->initHelperJs();
        $data['scriptsJs']=$scriptJs;
        $data['MesasController']=new MesasController();
        return view('Principales/principal', $data);
    }
}
