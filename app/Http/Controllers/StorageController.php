<?php

namespace App\Http\Controllers;

use Illuminate\Http\File;
use Illuminate\Support\Facades\File as FileFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StorageController extends Controller
{
    //
    //public static function guardarArchivo($archivo, $carpeta='sin_asignar')
    //{
    //    //obtenemos el nombre del archivo
    //    $nombre = $archivo['name'].uniqid();
    //
    //    //indicamos que queremos guardar un nuevo archivo en el disco local
    //    $directories = Storage::allDirectories();
    //    $crearDirectorio=true;
    //    foreach ($directories as $directory){
    //        if($directory==$carpeta)
    //            $crearDirectorio=false;
    //    }
    //    if($crearDirectorio)
    //        Storage::MakeDirectory($carpeta);
    //
    //    Storage::disk('local')->put($carpeta.'/'.$nombre,  new File($archivo), 'public');
    //
    //    return "archivo guardado";
    //}
    //
    public static function guardarArchivo(Request $request, $carpeta)
    {
        $file = $request->file('imagen-input');
        //obtenemos el nombre del archivo
        $nombre = uniqid().$file->getClientOriginalName();

        //indicamos que queremos guardar un nuevo archivo en el disco local
        $directories = Storage::allDirectories();
        $crearDirectorio=true;
        foreach ($directories as $directory){
            if($directory==$carpeta)
                $crearDirectorio=false;
        }
        if($crearDirectorio)
            Storage::MakeDirectory($carpeta);


        Storage::disk('local')->put($carpeta.'/'.$nombre,  FileFacade::get($file));

        return "archivo guardado";
    }
}
