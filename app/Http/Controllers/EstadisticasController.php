<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EstadisticasController extends Controller
{
    //

    public function index(){
        $this->initHelperCss();
        $libreriasJs=[asset('js/Chart/Chart.min.js'), asset('js/Propias/color-convert.js')];
        $scriptJs=$this->initHelperJs($libreriasJs);
        $data['color_array']=$this->color['color_array'];
        $data['scriptsJs']=$scriptJs;
        return view('Informacion/estadisticas', $data);
    }
}
