<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function __construct()
    {
        $this->middleware('auth');
        $this->color=config('colors');
    }

    /**
     * Agrega css a la vista
     *
     * @param array $pathes
     */
    public function initHelperCss($pathes=null){
        $css[]=asset('css/bootstrap.min.css');
        $css[]=asset('css/select2.min.css');
        $css[]=asset('css/propio.css');
        $css[]=asset('css/bootstrap-material-design.min.css');
        $css[]=asset('css/material+icons.css');
        if($pathes){
            $css=array_merge($css, $pathes);
        }
        foreach ($css as $cssOnly)
            echo "<link rel=\"stylesheet\" href=\"$cssOnly\" />";
    }

    public function initHelperJs($pathes=null){
        $js[]=asset('js/jquery-slim.min.js');
        $js[]=asset('js/jquery-3.4.1.js');
        $js[]=asset('js/bootstrap.min.js');
        $js[]=asset('js/popper.min.js');
        $js[]=asset('js/select2.min.js');
        $js[]=asset('js/bootstrap-material-design.js');
        $js[]=asset('js/Propias/Uploader.js');

        if($pathes){
            $js=array_merge($js, $pathes);
        }
        foreach ($js as $jsOnly)
            $script[]="<script src=\"$jsOnly\" type=\"text/javascript\"></script>";
        return $script;
    }


    public function getRoot(){
        if (isset($_SERVER['HTTPS'])) {
            $servidor='https://';
        }
        else{
            $servidor='http://';
        }
        return $servidor.request()->getHost().request()->getBaseUrl().'/';
    }

    public function passJsVariables(){
        $variables[]="<script type='text/javascript'>var root ='".$this->getRoot()."'</script>";
        $variables[]="<script type='text/javascript'>var csrfToken='".csrf_token()."'</script>";
        foreach ($variables as $variable){
            echo $variable;
        }
    }
}
