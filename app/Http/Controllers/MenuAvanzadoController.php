<?php

namespace App\Http\Controllers;

use App\Models\Guarnicion;
use App\Models\Intereses;
use App\Models\MateriaPrima;
use App\Models\Medicion;
use App\Models\Receta;
use App\Models\TipoMenu;
use App\Models\Usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \ReflectionMethod;

class MenuAvanzadoController extends Controller
{
    //
    public function index()
    {
        $this->initHelperCss();
        $this->passJsVariables();
        $js[] = asset('js/Propias/Menu-avanzado.js');
        $scriptJs = $this->initHelperJs($js);
        $data['scriptsJs'] = $scriptJs;
        $data['Controller'] =& $this;

        return view('Menu/menu-avanzado', $data);
    }

    public function getVistaTipoAgregar($tipoVista)
    {
        $data['Controller'] =& $this;
        $data['tipoVista'] = $tipoVista;

        return view('Menu-avanzado/vista-avanzada', $data);
    }

    public function getCategorias()
    {
        $tipoMenu = new TipoMenu();
        $tiposMenu = $tipoMenu->buscar([], [], true);
        $categorias['headers'] = ['#', 'nombre', 'activo'];
        foreach ($tiposMenu as $tipo) {
            $categorias[] = [
                'id'     =>
                    ['valor' => $tipo['id'], 'visible' => 0],
                'nombre' =>
                    ['valor' => $tipo['nombre'], 'visible' => 1],
                'activo' =>
                    ['valor' => $tipo['activo'], 'visible' => 1],
            ];
        }

        return $categorias ?: [];
    }

    public function getGuarniciones()
    {
        $guarnicionesObject = new Guarnicion();
        $todasGuarniciones = $guarnicionesObject->buscar([], [], true);
        $guarniciones['headers'] = ['#', 'nombre', 'categoria', 'precio', 'activo'];
        foreach ($todasGuarniciones as $guarnicion) {
            $guarniciones[] = [
                'id'               =>
                    ['valor' => $guarnicion['id'], 'visible' => 0],
                'nombre'           =>
                    ['valor' => $guarnicion['nombre'], 'visible' => 1],
                'tipo_menu'        =>
                    ['valor' => $guarnicion['tipo_menu_id'], 'visible' => 1],
                'precio'           =>
                    ['valor' => $guarnicion['precio'], 'visible' => 1],
                'activo'           =>
                    ['valor' => $guarnicion['activo'], 'visible' => 1],
            ];
        }

        return $guarniciones ?: [];
    }

    public function getMateriaPrima()
    {
        $materiaPrimaObject = new MateriaPrima();
        $todaMateriaPrima = $materiaPrimaObject->buscar([], [], true);
        $materia_prima['headers'] = ['#', 'nombre', 'cantidad', 'cantidad alerta', 'medicion', 'activo'];
        foreach ($todaMateriaPrima as $materiaPrima) {
            $materia_prima[] = [
                'id'              =>
                    ['valor' => $materiaPrima['id'], 'visible' => 0],
                'nombre'          =>
                    ['valor' => $materiaPrima['nombre'], 'visible' => 1],
                'cantidad'        =>
                    ['valor' => $materiaPrima['cantidad'], 'visible' => 1],
                'cantidad_alerta' =>
                    ['valor' => $materiaPrima['cantidad_alerta'], 'visible' => 1],
                'medicion'        =>
                    ['valor' => $materiaPrima->Medicion['nombre'] . " (" . $materiaPrima->Medicion['abreviatura'] . ")", 'visible' => 1],
                'activo'          =>
                    ['valor' => $materiaPrima['activo'], 'visible' => 1],
            ];
        }

        return $materia_prima ?: [];
    }

    public function getIntereses()
    {
        $interesesObject = new Intereses();
        $todosIntereses = $interesesObject->buscar([], [], true);
        $intereses['headers'] = ['#', 'nombre', 'porcentaje', 'activo'];
        foreach ($todosIntereses as $interes) {
            $intereses[] = [
                'id'         =>
                    ['valor' => $interes['id'], 'visible' => 0],
                'nombre'     =>
                    ['valor' => $interes['nombre'], 'visible' => 1],
                'porcentaje' =>
                    ['valor' => $interes['porcentaje'], 'visible' => 1],
                'activo'     =>
                    ['valor' => $interes['activo'], 'visible' => 1],
            ];
        }

        return $intereses ?: [];
    }

    public function getTasaDeMedicion()
    {
        $medicionObject = new Medicion();
        $todasMediciones = $medicionObject->buscar([], [], true);
        $mediciones['headers'] = ['#', 'nombre', 'abreviatura'];
        foreach ($todasMediciones as $medicion) {
            $mediciones[] = [
                'id'          =>
                    ['valor' => $medicion['id'], 'visible' => 0],
                'nombre'      =>
                    ['valor' => $medicion['nombre'], 'visible' => 1],
                'abreviatura' =>
                    ['valor' => $medicion['abreviatura'], 'visible' => 1],
            ];
        }

        return $mediciones ?: [];
    }

    public function getUsuarios()
    {
        $usuarioObject = new Usuarios();
        $todosUsuarios = $usuarioObject->buscar([], [], true);
        $usuario['headers'] = ['#', 'nombre', 'login', 'permisos', 'activo'];
        foreach ($todosUsuarios as $usuarios) {
            $usuario[] = [
                'id'       =>
                    ['valor' => $usuarios['id'], 'visible' => 0],
                'nombre'   =>
                    ['valor' => $usuarios['nombre'], 'visible' => 1],
                'login'    =>
                    ['valor' => $usuarios['login'], 'visible' => 1],
                'permisos' =>
                    ['valor' => $usuarios['email'], 'visible' => 1],
                'activo'   =>
                    ['valor' => $usuarios['activo'], 'visible' => 1],
            ];
        }

        return $usuario ?: [];
    }

    public function getVistaTabla($tipo)
    {
        $data['Controller'] =& $this;
        $data['tipo'] = $tipo;
        $titulo = strtolower(preg_replace('/([A-Z])/', ' $1', $tipo));
        $data['ver_vista'] = substr(strtolower(preg_replace('/([A-Z])/', '_$1', $tipo)), 1);
        $data['titulo'] = ucwords($titulo);

        return view('Menu-avanzado/vista-tabla', $data);
    }

    public function getVistaHeader($tipo)
    {
        $data['tipo'] = $tipo;
        $funcion = "get" . $tipo;
        ${$tipo} = $this->$funcion();
        foreach (${$tipo}['headers'] as $tipo) {
            $data['tabla'][] = $tipo;
        }

        return view('Menu-avanzado/vista-header', $data);
    }

    public function getVistaFila($tipo)
    {
        $data['tipo'] = $tipo;
        $titulo = strtolower(preg_replace('/([A-Z])/', ' $1', $tipo));
        $data['titulo'] = ucwords($titulo);
        $data['ver_vista'] = substr(strtolower(preg_replace('/([A-Z])/', '_$1', $tipo)), 1);
        $funcion = "get" . $tipo;
        //$reflectionMethod=new ReflectionMethod($this, $funcion);
        //${$tipo}=$reflectionMethod->invoke($this);
        ${$tipo} = $this->$funcion();
        unset(${$tipo}['headers']);
        $cont = 1;
        foreach (${$tipo} as $tipo) {
            $data['tabla'][$tipo['id']['valor']]['#'] = $cont++;
            foreach ($tipo as $key => $unit) {
                if ($unit['visible'] == 1)
                    $data['tabla'][$tipo['id']['valor']][$key] = $unit['valor'];
                else
                    $data['tabla'][$tipo['id']['valor']]['hide'][$key] = $unit['valor'];
            }
        }

        return view('Menu-avanzado/vista-fila', $data);
    }

    public function getVistaAgregarEdicion($tipo, $id)
    {
        $data['tipo'] = $tipo;
        $data['id'] = $id;
        if ($tipo == 'materia-prima') {
            $tasaDeMedicion = new Medicion();
            $mediciones = $tasaDeMedicion->buscar([], [], true);
            if (!count($mediciones) > 0)
                return 100;
            foreach ($mediciones as $medicion) {
                $data['mediciones'][$medicion['id']] = [
                    'id'                 => $medicion['id'],
                    'nombre_abreviatura' => $medicion['nombre'] . ' - ' . $medicion['abreviatura']];
            }
        } else if ($tipo == 'guarniciones') {
            $materiasPrimas = new MateriaPrima();
            $materiasPrima = $materiasPrimas->buscar([], [], true);
            $data['mostrar_agregar_materia_prima'] = (count($materiasPrima) > 0);
            foreach ($materiasPrima as $materiaPrima) {
                $data['materias_primas'][$materiaPrima['id']] = [
                    'id'          => $materiaPrima['id'],
                    'nombre'      => $materiaPrima['nombre'],
                    'medicion_id' => $materiaPrima['medicion_id'],
                    'medicion'    =>
                        ['abreviatura' => $materiaPrima->Medicion['abreviatura']],
                ];
            }
        }
        if ($id == 0) {

        } else {

        }

        return view('Menu-avanzado/popup-' . $tipo, $data);
    }

    public function agregarMateriaPrimaAVistaGuarnicion()
    {
        $datos = $_POST ?: $_GET;
        $materiasPrimas = new MateriaPrima();
        $where = [];
        if (!empty($datos['ids'])) {
            foreach ($datos['ids'] as $ids){
                $where[] = ['id','<>', $ids];
            }
        }
        $materiasPrima = $materiasPrimas->buscar([], $where, true);
        $data['id_unico'] = uniqid();
        foreach ($materiasPrima as $materiaPrima) {
            $data['materias_primas'][$materiaPrima['id']] = [
                'id'          => $materiaPrima['id'],
                'nombre'      => $materiaPrima['nombre'],
                'medicion_id' => $materiaPrima['medicion_id'],
                'medicion'    =>
                    ['abreviatura' => $materiaPrima->Medicion['abreviatura']],
            ];
        }

        return view('Menu-avanzado/materia-prima-guarnicion', $data);
    }

    public function guardarAvanzado($tipo, $id, $estado = null, Request $request)
    {
        $datos = $_POST ?: $_GET;
        if($_FILES){
            StorageController::guardarArchivo($request, $tipo);
        }
        switch ($tipo) {
            case 'tasa-de-medicion':
                $tadaDeMedicion = new Medicion();
                if (!is_null($estado)) {
                    $tadaDeMedicion['activo'] = $estado;
                } else {
                    $tadaDeMedicion['nombre'] = $datos['nombre'];
                    $tadaDeMedicion['abreviatura'] = $datos['abreviatura'];
                }
                $tadaDeMedicion->guardar(['id' => $id]);
                break;
            case 'intereses':
                $intereses = new Intereses();
                if (!is_null($estado)) {
                    $intereses['activo'] = $estado;
                } else {
                    $intereses['nombre'] = $datos['nombre'];
                    $intereses['porcentaje'] = $datos['porcentaje'];
                    $intereses['activo'] = !empty($datos['activo']) ? 1 : 0;
                }
                $intereses->guardar(['id' => $id]);
                break;
            case 'materia-prima':
                $materiaPrima = new MateriaPrima();
                if (!is_null($estado)) {
                    $materiaPrima['activo'] = $estado;
                } else {
                    $materiaPrima['nombre'] = $datos['nombre'];
                    $materiaPrima['cantidad'] = $datos['cantidad'];
                    $materiaPrima['medicion_id'] = $datos['medicion_id'];
                    $materiaPrima['cantidad_alerta'] = $datos['cantidad_alerta'];
                    $materiaPrima['activo'] = !empty($datos['activo']) ? 1 : 0;
                }
                $materiaPrima->guardar(['id' => $id]);
                break;
            case 'guarniciones':
                $guarniciones = new Guarnicion();
                if (!is_null($estado)) {
                    $guarniciones['activo'] = $estado;
                } else {
                    $guarniciones['nombre'] = $datos['nombre'];
                    $guarniciones['imagen'] = "1";
                    $guarniciones['tipo_menu_id'] = 1;
                    $guarniciones['precio'] = 1;
                    $guarniciones['precio_adicional'] = 1;
                    $guarniciones['detalle'] = 1;
                    $guarniciones['activo'] = !empty($datos['activo']) ? 1 : 0;
                }
                $guarnicion = $guarniciones->guardar(['id' => $id]);
                if (!empty($datos['materias_primas'])) {
                    $recetas = new Receta();
                    $recetas['class'] = 'Guarnicion';
                    $recetas['class_id'] = $guarnicion->id;
                    foreach ($datos['materias_primas'] as $materias_primas) {
                        $recetas['materia_prima_id'] = $materias_primas['id'];
                        $recetas['cantidad_materia_prima'] = $materias_primas['cantidad_materia_prima'];
                        $recetas['activo'] = 1;//verificar activo
                        $recetas->guardar();
                    }
                }
                break;
            case 'categorias':
                $tipoMenu = new TipoMenu();
                if (!is_null($estado)) {
                    $tipoMenu['activo'] = $estado;
                } else {
                    $tipoMenu['nombre'] = $datos['nombre'];
                    $tipoMenu['padre_id'] = 0;
                    $tipoMenu['activo'] = !empty($datos['activo']) ? 1 : 0;
                }
                $tipoMenu->guardar(['id' => $id]);
                break;
        }
        var_dump($tipo);
    }

}
