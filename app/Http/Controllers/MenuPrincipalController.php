<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Promociones;
use Illuminate\Http\Request;

class MenuPrincipalController extends Controller
{
    //
    public function index(){
        $this->initHelperCss();
        $this->passJsVariables();
        $data['Controller']=&$this;
        return view('Menu/menu-principal',$data);
    }

    public function getPlatosActivos($esMenu=false, $id_mesa=0){
        //busqueda de platos
        $libreriasJs[]=asset('js/Propias/Platos.js');
        $libreriasJs[]=asset('js/Propias/Ordenes.js');
        $scriptJs=$this->initHelperJs($libreriasJs);
        $data['scriptsJs']=$scriptJs;
        $data['Controller']=&$this;
        $data['esMenu']=$esMenu?1:0;
        $data['id_mesa']=$id_mesa;
        $platosPorCategoria=$this->getPlatosPorCategoria();
        $data['mostrar_categoria']=($platosPorCategoria);
        $promociones=$this->getPromos();
        $data['mostrar_promo']=($promociones);
        if(count($platosPorCategoria) == 0){
            $data['categorias']=[];
        }
        foreach ($platosPorCategoria as $key=>$platoPorCategoria){
            $data['categorias'][$key]['id']=$platoPorCategoria['id'];
            $data['categorias'][$key]['nombre']=$platoPorCategoria['nombre'];
        }
        return view('Menu-principal/vista-platos', $data);
    }

    public function getVistaPlatosPorCategorias($categoriaId=0, $esMenu=false, $id_mesa){
        $data['categorias']=$this->getPlatosPorCategoria($categoriaId);
        $data['esMenu']=$esMenu;
        $data['id_mesa']=$id_mesa;

        return view('Menu-principal/vista-platos-por-categorias', $data);
    }

    public function getVistaPromociones($esMenu=false, $id_mesa){
        $data['promociones']=$this->getPromos();
        $data['esMenu']=$esMenu;
        $data['id_mesa']=$id_mesa;

        return view('Menu-principal/vista-platos-promociones', $data);
    }

    public function getPlatosPorCategoria($categoriaId=0){
        $menu = new Menu();
        $categorias=[];
        if($categoriaId==0){
            $platos=$menu->buscar();
            foreach ($platos as $plato){
                $categorias[$plato->TipoMenu['id']]['id']=$plato->TipoMenu['id'];
                $categorias[$plato->TipoMenu['id']]['nombre']=$plato->TipoMenu['nombre'];
                $categorias[$plato->TipoMenu['id']]['platos'][$plato['id']]['id']=$plato['id'];
                $categorias[$plato->TipoMenu['id']]['platos'][$plato['id']]['nombre']=$plato['nombre'];
                $categorias[$plato->TipoMenu['id']]['platos'][$plato['id']]['imagen']=$plato['imagen'];
            }
        } else if ($categoriaId==-1){
            //busco los que no tengan categorias
            $platos=$menu->buscar([],['tipo_menu_id' => 0]);
            foreach ($platos as $plato){
                $categorias['1']['id']=-1;
                $categorias['1']['nombre']="No Asignados";
                $categorias['1']['platos'][$plato['id']]['id']=$plato['id'];
                $categorias['1']['platos'][$plato['id']]['nombre']=$plato['nombre'];
                $categorias['1']['platos'][$plato['id']]['imagen']=$plato['imagen'];
            }
        } else {
            $platos=$menu->buscar([],['activo' => 1,'tipo_menu_id' => $categoriaId]);
            foreach ($platos as $plato){
                $categorias[$plato->TipoMenu['id']]['id']=$plato->TipoMenu['id'];
                $categorias[$plato->TipoMenu['id']]['nombre']=$plato->TipoMenu['nombre'];
                $categorias[$plato->TipoMenu['id']]['platos'][$plato['id']]['id']=$plato['id'];
                $categorias[$plato->TipoMenu['id']]['platos'][$plato['id']]['nombre']=$plato['nombre'];
                $categorias[$plato->TipoMenu['id']]['platos'][$plato['id']]['imagen']=$plato['imagen'];
            }
        }
        return $categorias;
    }

    public function getPromos(){
        $promos = new Promociones();
        $promociones=[];
        $promosActivas=$promos->buscar();
        foreach ($promosActivas as $promoActiva){
            $promociones[$promoActiva['id']]['id']=$promoActiva['id'];
            $promociones[$promoActiva['id']]['nombre']=$promoActiva['nombre'];
            $promociones[$promoActiva['id']]['precio']=$promoActiva['precio'];
            $promociones[$promoActiva['id']]['imagen']=$promoActiva['imagen'];
            foreach ($promoActiva->MenuPromociones as $menuPromociones){
                if($menuPromociones->Menu){
                    $promociones[$promoActiva['id']]['menu_promociones'][$menuPromociones->Menu['id']]['id']=$menuPromociones->Menu['id'];
                    $promociones[$promoActiva['id']]['menu_promociones'][$menuPromociones->Menu['id']]['nombre']=$menuPromociones->Menu['nombre'];
                    $promociones[$promoActiva['id']]['menu_promociones'][$menuPromociones->Menu['id']]['cantidad']=$menuPromociones['cantidad'];
                    if($menuPromociones['incluye_guarnicion'] && $menuPromociones->menu['tiene_guarnicion']){
                        foreach ($menuPromociones->menu->MenuGuarnicion as $menuGuarnicion){
                            $promociones[$promoActiva['id']]['menu_promociones'][$menuPromociones->Menu['id']]
                            ['Guarniciones'][$menuGuarnicion['id']]['id']=$menuGuarnicion->Guarnicion['id'];
                            $promociones[$promoActiva['id']]['menu_promociones'][$menuPromociones->Menu['id']]
                            ['Guarniciones'][$menuGuarnicion['id']]['nombre']=$menuGuarnicion->Guarnicion['nombre'];
                        }
                    }
                }
                if ($menuPromociones->Guarnicion){
                    $promociones[$promoActiva['id']]['menu_promociones'][$menuPromociones->Guarnicion['id']]['id']=$menuPromociones->Guarnicion['id'];
                    $promociones[$promoActiva['id']]['menu_promociones'][$menuPromociones->Guarnicion['id']]['nombre']=$menuPromociones->Guarnicion['nombre'];
                    $promociones[$promoActiva['id']]['menu_promociones'][$menuPromociones->Guarnicion['id']]['cantidad']=$menuPromociones['cantidad'];
                }
            }
        }
        return $promociones;
    }

    public function getDescripcionPlatoOPromocion($idPlatoOPromocion=0, $id_mesa=0){
        //if($tipoPlatoOPromocion=='plato'){
        $data['id_mesa']=$id_mesa;
            $data['idplato']=$idPlatoOPromocion;
            $data['plato']=[
                'id' => 2,
                'nombre' => 'arroz con menestra y carne',
                'precio' => 6.50,
                'detalle' => 'asddddddddd ddddddd dddd ddddd ddddddddd dddddd
            asddd dddddd ddddddddd ddddddddd ddddddddd ddddddddddd ddddddddddd ddddddddd dddddddddd
            adsasd asda sdasda sdas dasdasdd dddddddd ddddd ddddddddd ddddddd ddd ddd ddddddd dddddddd dda
            asdddddd dddd ddddddd ddddds sssss sssssssss ssssss sssssssss ssssss sssssss sssssssss sssss
            dsd dddd ddddd ddddd dddddd dddddd dddddd ddddddd dddddddd dddd dddddd ddd',
                'imagen'=> 'asdasda',
                'Guarniciones' => [[
                    'id' => 1,
                    'nombre' => 'menestra',
                    'tipo_cantidad' => 'Gr',
                    'cantidad' => 100,
                    'adicional' => 2
                ],[
                    'id' => 2,
                    'nombre' => 'arroz',
                    'tipo_cantidad' => 'Lb',
                    'cantidad' => 1,
                    'adicional' => 4
                ],[
                    'id' => 3,
                    'nombre' => 'carne',
                    'tipo_cantidad' => 'Lb',
                    'cantidad' => 1,
                    'adicional' => 8
                ]],
                'Materia_Prima' => [[
                    'nombre' => 'menestra',
                    'tipo_cantidad' => 'Gr',
                    'cantidad' => 100
                ],[
                    'nombre' => 'arroz',
                    'tipo_cantidad' => 'Lb',
                    'cantidad' => 1
                ],[
                    'nombre' => 'carne',
                    'tipo_cantidad' => 'Lb',
                    'cantidad' => 1
                ]]
            ];
        //}
        //else {
        //    $data['idPromo']=$idPlatoOPromocion;
        //    $data['promo']=[
        //        'id' => 1,
        //        'nombre' => '2 X 1 parrillada Doble',
        //        'precio' => 15.50,
        //        'imagen'=> 'asdasda',
        //        'activo' => 1,
        //        'menu_promociones' => [[
        //            'id' => 1,
        //            'nombre' => 'parrillada doble',
        //            'cantidad' => 2,
        //            'Guarniciones' => [[
        //                'id' => 1,
        //                'nombre' => 'menestra',
        //                'tipo_cantidad' => 'Gr',
        //                'cantidad' => 100,
        //                'adicional' => 0
        //            ],[
        //                'id' => 3,
        //                'nombre' => 'carne',
        //                'tipo_cantidad' => 'Lb',
        //                'cantidad' => 1,
        //                'adicional' => 3
        //            ]],
        //        ]]
        //    ];
        //}

        return view('Menu-principal/vista-detalle-plato-promo', $data);
    }

    public function guardarPlato(){
        $datos=$_POST?:$_GET;
        $data['message']="Exito al Guardar";
        return redirect()->route('menu-principal')->with($data);
    }

    public function vistaAgregarPlato(){
        $datos=$_POST?:$_GET;
        $data['Controller']=&$this;

        if($datos['idPlatoPromo']==0){
            $data['plato_activo']=true;
            $data['nuevo']=true;
        } else {
            $data['nuevo']=false;
            $data['plato_activo']=$datos['tipo']=='plato';
            $data['categorias'][]=[
                'id' => 1,
                'nombre' => 'principal'
            ];
            $data['categorias'][]=[
                'id' => 2,
                'nombre' => 'colas',
                'hijos' => [
                    'id' => 3,
                    'nombre' => 'Coca Cola Company'
                ]
            ];
        }
        return view('Menu-principal/agregar-menu', $data);
    }

    public function getVistaTipoAgregar($TipoVista='plato'){
        return view('Menu-principal/agregar-editar-'.$TipoVista);
    }
}
