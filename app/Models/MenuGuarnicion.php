<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MenuGuarnicion extends GeneralModel
{
    //
    protected $table='menu_guarnicion';
    protected $fillable = ['guarnicion_id', 'fecha_creacion', 'activo'];

    public function Guarnicion(){
        return $this->belongsTo(Guarnicion::class, 'guarnicion_id','id');
    }
}