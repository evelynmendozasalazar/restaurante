<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guarnicion extends GeneralModel
{
    //
    protected $table='guarnicion';
    protected $fillable = ['nombre', 'precio', 'fecha_creacion', 'activo'];

    public function Receta(){
        return $this->hasMany(Receta::class, 'id','guarnicion_id');
    }

}
