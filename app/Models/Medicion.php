<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Medicion extends GeneralModel
{
    //
    public $timestamps = false;

    protected $table='medicion';
    protected $fillable = ['nombre', 'abreviatura'];

}
