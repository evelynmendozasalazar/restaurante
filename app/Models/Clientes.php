<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clientes extends GeneralModel
{
    //
    protected $table='clientes';
    protected $fillable = ['cedula', 'nombre', 'fecha_nacimiento', 'fecha_creacion', 'activo'];
}
