<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Receta extends GeneralModel
{
    //
    public $timestamps = false;

    protected $table='receta';
    protected $fillable = ['class', 'class_id', 'materia_prima_id', 'cantidad_materia_prima', 'activo'];

    public function Menu(){
        return $this->belongsTo(Menu::class, 'class_id')->where('class', 'Menu');
    }

    public function Guarnicion(){
        return $this->belongsTo(Guarnicion::class, 'class_id')->where('class', 'Guarnicion');
    }

    public function MateriaPrima(){
        return $this->belongsTo(MateriaPrima::class, 'materia_prima_id', 'id');
    }
}
