<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Permisos extends GeneralModel
{
    //
    protected $table='permisos';
    protected $fillable = ['tipo_usuario_id', 'permiso'];
}
