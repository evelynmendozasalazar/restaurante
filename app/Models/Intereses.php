<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Intereses extends GeneralModel
{
    //
    public $timestamps = false;

    protected $table='intereses';
    protected $fillable = ['nombre', 'porcentaje', 'activo'];

}
