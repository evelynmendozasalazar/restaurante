<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class RecetaMateriaPrima extends GeneralModel
{
    //
    protected $table='receta_materia_prima';
    protected $fillable = ['receta_id', 'materia_prima_id', 'cantidad'];
}
