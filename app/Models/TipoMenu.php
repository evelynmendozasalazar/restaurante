<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class TipoMenu extends GeneralModel
{
    //
    public $timestamps=false;
    protected $table='tipo_menu';
    protected $fillable = ['nombre', 'padre_id', 'activo'];

    public function TipoMenuPadre(){
        return $this->belongsTo($this, 'padre_id', 'id');
    }

    public function TipoMenuHijo(){
        return $this->hasMany($this, 'id', 'padre_id');
    }

    public function Menu(){
        return $this->hasMany(Menu::class, 'tipo_menu_id');
    }
}
